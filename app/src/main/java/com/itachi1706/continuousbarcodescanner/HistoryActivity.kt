package com.itachi1706.continuousbarcodescanner

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_history.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        val string = sp.getString("barcodehist", "null")
        if (string == "null") {
            AlertDialog.Builder(this).setTitle("No History")
                .setMessage("You have no barcode history").setCancelable(false)
                .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int -> setResult(Activity.RESULT_CANCELED); finish()}.show()
            return
        }

        val gson = Gson()
        val barcodeHistory = gson.fromJson<ArrayList<BarcodeHistory>>(string, object : TypeToken<ArrayList<BarcodeHistory>>() {}.type)
        barcodeHistory.reverse()
        val list = ArrayList<DualLineString>()
        val sdf = SimpleDateFormat("dd/MM/yy HH:mm:ss zzz", Locale.US)
        val dt = Date()
        barcodeHistory.forEach{
            dt.time = it.timestamp
            list.add(DualLineString(sdf.format(dt), "Barcodes: ${it.barcodes.size}", it))
        }
        val adapter = DualLineStringRecyclerAdapter(list, false)
        adapter.setOnClickListener{
            val viewHolder = it.tag as DualLineStringRecyclerAdapter.StringViewHolder
            val pos = viewHolder.adapterPosition
            val barcodes = list[pos].extra as BarcodeHistory
            dt.time = barcodes.timestamp
            AlertDialog.Builder(this).setTitle("Import this list of barcodes?")
                .setMessage("Time Taken: ${sdf.format(dt)}\n\nBarcodes:\n${barcodes.barcodes.joinToString("\n")}")
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    val result = Intent()
                    result.putExtra("barcode", gson.toJson(barcodes.barcodes))
                    setResult(Activity.RESULT_OK, result)
                    finish()
                }.setNegativeButton(android.R.string.cancel, null)
                .setNeutralButton(R.string.delete){ _: DialogInterface, _: Int ->
                    list.removeAt(pos)
                    barcodeHistory.remove(barcodes)
                    sp.edit().putString("barcodehist", gson.toJson(barcodeHistory)).apply()
                    adapter.update(list)
                    adapter.notifyDataSetChanged()
                    Toast.makeText(applicationContext, "Removed Barcode History Record", Toast.LENGTH_LONG).show()
                }.show()
        }

        recycler_view.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler_view.layoutManager = linearLayoutManager
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED)
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
