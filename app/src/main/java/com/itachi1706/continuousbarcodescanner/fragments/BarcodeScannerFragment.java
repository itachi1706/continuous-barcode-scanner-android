package com.itachi1706.continuousbarcodescanner.fragments;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itachi1706.continuousbarcodescanner.BarcodeCaptureActivity;
import com.itachi1706.continuousbarcodescanner.BarcodeHistory;
import com.itachi1706.continuousbarcodescanner.HistoryActivity;
import com.itachi1706.continuousbarcodescanner.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.DEVICE_POLICY_SERVICE;

/**
 * Created by Kenneth on 24/12/2017.
 * for com.itachi1706.cheesecakeutilities.Modules.BarcodeTools.Fragments in CheesecakeUtilities
 */

public class BarcodeScannerFragment extends Fragment {

    // use a compound button so either checkbox or switch widgets work.
    private CompoundButton useFlash;
    private Button scan;
    private TextView statusMessage;
    private TextView barcodeValue;

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeScanner";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_barcode_scanner, container, false);

        statusMessage = v.findViewById(R.id.status_message);
        barcodeValue = v.findViewById(R.id.barcode_value);
        barcodeValue.setMovementMethod(new ScrollingMovementMethod());
        useFlash = v.findViewById(R.id.use_flash);
        scan = v.findViewById(R.id.read_barcode);
        scan.setOnClickListener(view -> {
            // launch barcode activity.
            Intent intent = new Intent(getActivity(), BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.USE_FLASH, useFlash.isChecked());
            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        });

        if (savedInstanceState != null && savedInstanceState.containsKey("barcodes")) {
            barcodeStrings = savedInstanceState.getStringArrayList("barcodes");
            updateBarcodes();
        }
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Check for camera source
        //noinspection ConstantConditions
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getActivity().getSystemService(DEVICE_POLICY_SERVICE);
        if (!this.getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA) ||
                (devicePolicyManager != null && devicePolicyManager.getCameraDisabled(null))) {
            // Disables stuff
            scan.setEnabled(false);
            statusMessage.setText(R.string.no_camera_hardware);
        } else {
            // Enable stuff
            scan.setEnabled(true);
            if (!updateStatus) statusMessage.setText(getString(R.string.barcode_header));
            else updateStatus = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        updateStatus = false;
    }

    private boolean updateStatus = false;


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (barcodeStrings != null && !barcodeStrings.isEmpty()) outState.putStringArrayList("barcodes", barcodeStrings);
    }

    private ArrayList<String> barcodeStrings;

    private void updateBarcodes() {
        statusMessage.setText(R.string.barcode_success);
        updateStatus = true;
        String result = String.join("\n", barcodeStrings);
        barcodeValue.setText(result);
        barcodeValue.setClickable(true);
        barcodeValue.setOnLongClickListener(v1 -> {
            ClipData clip = ClipData.newPlainText("barcode", result);
            View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v1);
            v1.startDragAndDrop(clip, dragShadowBuilder, true, View.DRAG_FLAG_GLOBAL | View.DRAG_FLAG_GLOBAL_URI_READ |
                    View.DRAG_FLAG_GLOBAL_PERSISTABLE_URI_PERMISSION);
            return true;
        });
        barcodeValue.setOnClickListener(v -> {
            if (barcodeStrings != null && !barcodeStrings.isEmpty()) {
                registerForContextMenu(barcodeValue);
                barcodeValue.showContextMenu();
                unregisterForContextMenu(barcodeValue);
            }
        });
    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (getActivity() == null) return;
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.export_options, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ex_clipboard:
                if (getContext() == null) {
                    Log.e(TAG, "An error occurred (copy)");
                    return true;
                }
                if (barcodeStrings == null || barcodeStrings.isEmpty()) {
                    Toast.makeText(getContext(), "No barcodes found, Scan some now!", Toast.LENGTH_LONG).show();
                    return true;
                }
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard != null) {
                    ClipData clip = ClipData.newPlainText("barcode", String.join("\n", barcodeStrings));
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getContext(), "Barcode copied to clipboard", Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.ex_share:
                if (getContext() == null) {
                    Log.e(TAG, "An error occurred (share)");
                    return true;
                }
                if (barcodeStrings == null || barcodeStrings.isEmpty()) {
                    Toast.makeText(getContext(), "No barcodes to share", Toast.LENGTH_LONG).show();
                    return true;
                }
                String barcode = String.join("\n", barcodeStrings);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, barcode);
                startActivity(Intent.createChooser(share, getResources().getString(R.string.share_barcodes_chooser)));
                return true;
            case R.id.ex_save_file:
                if (getContext() == null) {
                    Log.e(TAG, "An error occurred (save)");
                    return true;
                }
                if (barcodeStrings == null || barcodeStrings.isEmpty()) {
                    Toast.makeText(getContext(), "No barcodes to save to file", Toast.LENGTH_LONG).show();
                    return true;
                }

                // Export
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS", Locale.US);
                String defaultFileName = "scanned-barcodes-" + sdf.format(new Date()) + ".txt";
                Intent exportIntent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                exportIntent.addCategory(Intent.CATEGORY_OPENABLE);
                exportIntent.setType("text/plain");
                exportIntent.putExtra(Intent.EXTRA_TITLE, defaultFileName);
                startActivityForResult(exportIntent, EXPORT_BARCODES);
                return true;
            default: return super.onContextItemSelected(item);
        }
    }

    private static final int EXPORT_BARCODES = 4;

    // Returning barcode activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Gson gson = new Gson();
                    String json = data.getStringExtra(BarcodeCaptureActivity.BARCODE_ARRAY);
                    if (json == null) return;
                    barcodeStrings = gson.fromJson(json, new TypeToken<ArrayList<String>>(){}.getType());
                    updateBarcodes();
                    if (getContext() != null) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
                        ArrayList<BarcodeHistory> history;
                        String msg = sp.getString("barcodehist", "null");
                        history = (msg.equals("null")) ? new ArrayList<>() : gson.fromJson(msg, new TypeToken<ArrayList<BarcodeHistory>>(){}.getType());
                        history.add(new BarcodeHistory(System.currentTimeMillis(), barcodeStrings));
                        sp.edit().putString("barcodehist", gson.toJson(history)).apply();
                    }
                    Log.d(TAG, "Barcode parsed");
                } else {
                    statusMessage.setText(R.string.barcode_failure);
                    barcodeValue.setClickable(false);
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                statusMessage.setText(String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else if (requestCode == EXPORT_BARCODES && resultCode == Activity.RESULT_OK && data != null && data.getData() != null && getContext() != null) {
            Uri uri = data.getData();
            try {
                OutputStream os = getContext().getContentResolver().openOutputStream(uri);
                if (os == null) {
                    Toast.makeText(getContext(), "Error obtaining output stream to save to file", Toast.LENGTH_LONG).show();
                    return;
                }
                if (barcodeStrings == null || barcodeStrings.isEmpty()) {
                    Toast.makeText(getContext(), "An error occurred writing barcodes to file", Toast.LENGTH_LONG).show();
                    return;
                }
                String textToWrite = String.join("\n", barcodeStrings);
                PrintWriter pw = new PrintWriter(os);
                pw.print(textToWrite);
                pw.close();
                os.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG, "Failed to save to file");
                Toast.makeText(getContext(), "An error occurred saving to file (FileNotFound)", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "Failed to close output");
                Toast.makeText(getContext(), "An error occurred saving to file (IOException)", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_HIST && resultCode == Activity.RESULT_OK) {
            Gson gson = new Gson();
            String msg = data.getStringExtra("barcode");
            if (msg != null && !msg.isEmpty()) {
                barcodeStrings = gson.fromJson(msg, new TypeToken<ArrayList<String>>(){}.getType());
                if (barcodeStrings == null) barcodeStrings = new ArrayList<>();
                updateBarcodes();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_scanner, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_history) {
            startActivityForResult(new Intent(getActivity(), HistoryActivity.class), REQUEST_HIST);
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    private static final int REQUEST_HIST = 1;
}
