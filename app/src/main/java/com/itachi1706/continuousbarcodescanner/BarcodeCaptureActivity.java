/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.itachi1706.continuousbarcodescanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.gson.Gson;
import com.itachi1706.continuousbarcodescanner.mlkit.barcode.BarcodeGraphic;
import com.itachi1706.continuousbarcodescanner.mlkit.barcode.BarcodeScanningProcessor;
import com.itachi1706.continuousbarcodescanner.mlkit.camera.CameraSource;
import com.itachi1706.continuousbarcodescanner.mlkit.camera.CameraSourcePreview;
import com.itachi1706.continuousbarcodescanner.mlkit.camera.GraphicOverlay;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Activity for the multi-tracker app.  This app detects barcodes and displays the value with the
 * rear facing camera. During detection overlay graphics are drawn to indicate the position,
 * size, and ID of each barcode.
 */
public final class BarcodeCaptureActivity extends AppCompatActivity implements GraphicOverlay.OnBarcodeDetectedListener {
    private static final String TAG = "Barcode-reader";

    // permission request codes need to be < 256
    private static final int RC_CAMERA_PERMISSION = 1;

    // constants used to pass extra data in the intent
    public static final String BARCODE_ARRAY = "BarcodeArray";
    public static final String USE_FLASH = "UseFlash";

    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;

    // helper objects for detecting taps and pinches.
    private boolean useFlash;

    private Snackbar doneded;

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.barcode_capture);

        mPreview = findViewById(R.id.preview);
        if (mPreview == null) Log.d(TAG, "Preview is null");
        mGraphicOverlay = findViewById(R.id.graphicOverlay);
        if (mGraphicOverlay == null) Log.d(TAG, "graphicOverlay is null");

        useFlash = getIntent().getBooleanExtra(USE_FLASH, false);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(useFlash); // Barcode Detected
        } else {
            requestCameraPermission();
        }

        doneded = Snackbar.make(mGraphicOverlay, "Barcodes Scanned: 0", Snackbar.LENGTH_INDEFINITE).setAction("Finish", v -> {
            doneded.dismiss();
            Gson gson = new Gson();
            String barcodes = gson.toJson(barcodesDetected);
            Intent data = new Intent();
            data.putExtra(BARCODE_ARRAY, barcodes);
            setResult(CommonStatusCodes.SUCCESS, data);
            finish();
        });
        doneded.show();

        if (icicle != null && icicle.containsKey("barcode")) {
            barcodesDetected = icicle.getStringArrayList("barcode");
            if (barcodesDetected == null) barcodesDetected = new ArrayList<>();
            doneded.setText("Barcode Scanned: " + barcodesDetected.size());
        }
    }

    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean useFlash) {
        // If there's no existing cameraSource, create one.
        if (mCameraSource == null) {
            mCameraSource = new CameraSource(this, mGraphicOverlay, useFlash);
        }

        mCameraSource.setMachineLearningFrameProcessor(new BarcodeScanningProcessor());
    }

    /**
     * Starts or restarts the camera source, if it exists. If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {
        if (mCameraSource != null) {
            try {
                if (mPreview == null) {
                    Log.d(TAG, "resume: Preview is null");
                    return;
                }
                if (mGraphicOverlay == null) {
                    Log.d(TAG, "resume: graphOverlay is null");
                    return;
                }
                mPreview.start(mCameraSource, mGraphicOverlay);
                mGraphicOverlay.setBarcodeDetectedListener(this);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        startCameraSource();
    }

    /** Stops the camera. */
    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_CAMERA_PERMISSION);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = view -> ActivityCompat.requestPermissions(thisActivity, permissions,
                RC_CAMERA_PERMISSION);

        findViewById(R.id.topLayout).setOnClickListener(listener);
        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != RC_CAMERA_PERMISSION) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource(useFlash);
            return;
        }

        logPermError(grantResults);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Barcode Scanner")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, (dialog, id) -> finish())
                .show();
    }



    public static void logPermError(@NonNull int[] grantResults) {
        Log.e("PermMan", "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));
    }

    @Override
    public void onDetectBarcode(BarcodeGraphic barcode) {
        FirebaseVisionBarcode firebaseBarcode = barcode.getBarcode();
        if (firebaseBarcode == null) return;
        String value = firebaseBarcode.getDisplayValue();
        if (!barcodesDetected.contains(value)) {
            barcodesDetected.add(value);
            doneded.setText("Barcodes Scanned: " + barcodesDetected.size() + "\nLast Scanned: " + value);
            ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            toneGen1.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD,150);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (!barcodesDetected.isEmpty()) outState.putStringArrayList("barcode", barcodesDetected);
    }

    ArrayList<String> barcodesDetected = new ArrayList<>();
}