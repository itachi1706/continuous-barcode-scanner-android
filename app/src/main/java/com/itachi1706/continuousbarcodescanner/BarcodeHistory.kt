package com.itachi1706.continuousbarcodescanner

/**
 * Created by Kenneth on 11/10/2019.
 * for com.itachi1706.continuousbarcodescanner in Continuous Barcode Scanner
 */
data class BarcodeHistory(val timestamp: Long = System.currentTimeMillis(), val barcodes: ArrayList<String> = ArrayList())